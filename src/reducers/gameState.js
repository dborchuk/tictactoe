import { fromJS } from 'immutable';

const defaultState = fromJS({
  boardState: '000000000',
  nextKey: '1',
  winner: null,
  isDraw: false
});

const gameState = (state = defaultState, action) => {
  switch (action.type) {

    case 'GAME_STEP':
      return state.merge(gameStep(state.toJS(), action));

    case 'NEW_GAME':
      return state.merge(defaultState);

    default:
      return state;
  }
}

export default gameState;

const replaceAt = (str, index, replacement) => {
  return str.substr(0, index) + replacement + str.substr(index + replacement.length);
}

const detectWinner = (state) => {
  const winCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < winCombinations.length; i++) {
    const [a, b, c] = winCombinations[i];
    if (state[a] !== '0' && state[a] === state[b] && state[a] === state[c]) {
      return state[a];
    }
  }

  return null;
}

const gameStep = (state, action) => {
  let newBoardState = state.boardState;
  let winner = state.winner;
  let nextKey = state.nextKey;
  let isDraw = false;

  if (state.boardState[action.payload.index] === '0') {
    newBoardState = replaceAt(state.boardState, action.payload.index, state.nextKey)
    winner = detectWinner(newBoardState);
    nextKey = state.nextKey === '1' ? '2' : '1';
  }

  if (!winner) {
    isDraw = detectDraw(newBoardState);
  }

  return {
    boardState: newBoardState,
    nextKey,
    winner,
    isDraw
  };
}

const detectDraw = (boardState) => {
  for (let i = 0; i < boardState.length; i++) {
    if (boardState[i] === '0') {
      return false;
    }
  }

  return true;
}
