import { fromJS } from 'immutable';

const defaultState = fromJS({
  isOpened: true
});

const menu = (state = defaultState, action) => {
  switch (action.type) {
    case 'CLOSE_MENU':
      return state.merge({
        isOpened: false,
      });
    default:
      return state;
  }
}

export default menu;
