import { combineReducers } from 'redux-immutable';

import gameState from './gameState';
import menu from './menu';

const reducers = combineReducers({
  gameState,
  menu
});

export default reducers;
