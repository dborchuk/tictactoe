import { connect } from 'react-redux';

import { setBoardState } from '../../actions/board';
import Board from '../../components/board';

const mapStateToProps = (state) => {
  const game = state.get('gameState').toJS();

  return {
    state: game.boardState,
    nextKey: game.nextKey
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChange: (index, key) => {
      dispatch(setBoardState(index, key));
    }
  }
}

const BoardContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Board);

export default BoardContainer;
