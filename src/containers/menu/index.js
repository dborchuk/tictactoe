import { connect } from 'react-redux';

import { newGame, closeMenu } from '../../actions/menu';
import Menu from '../../components/menu';

const mapStateToProps = (state) => {
  const game = state.get('gameState').toJS();

  return {
    winner: game.winner,
    isDraw: game.isDraw
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    newGameClick: () => {
      dispatch(newGame());
      dispatch(closeMenu());
    }
  }
}

const MenuContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Menu);

export default MenuContainer;
