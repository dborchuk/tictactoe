import { connect } from 'react-redux';

import Game from '../../components/game';

const mapStateToProps = (state) => {
  const game = state.get('gameState').toJS();
  const menu = state.get('menu').toJS();

  return {
    menuIsOpened: menu.isOpened,
    winner: game.winner,
    isDraw: game.isDraw
  };
};

const mapDispatchToProps = (dispatch) => {
  return {}
}

const GameContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Game);

export default GameContainer;
