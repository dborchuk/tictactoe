export const setBoardState = (index, key) => {
  return {
    type: 'GAME_STEP',
    payload: {
      index
    }
  };
}
