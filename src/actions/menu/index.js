export const newGame = () => {
  return {
    type: 'NEW_GAME'
  };
}

export const closeMenu = () => {
  return {
    type: 'CLOSE_MENU'
  };
}
