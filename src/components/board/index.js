import React from 'react';
import PropTypes from 'prop-types';

import Cell from '../cell'
import './styles.css';

function Board(props) {
  const cellNumber = 9;
  let cells = [];

  for (let i = 0; i < cellNumber; i++) {
    let elementKey = props.state.slice(i, i + 1);
    let onClick = (index) => {
      props.onChange(index, '1');
    };
    cells.push(<Cell key={i} elementKey={elementKey} onClick={onClick.bind(null, i)}></Cell>);
  }

  return <div className="container">
    <div className="next">
      Next: <Cell elementKey={props.nextKey}></Cell>
    </div>
    <div className="board">
      {cells}
    </div>
  </div>;
}

Board.propTypes = {
  onChange: PropTypes.func.isRequired,
  state: PropTypes.string.isRequired,
  nextKey: PropTypes.string.isRequired
}

export default Board;
