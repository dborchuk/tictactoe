import React from 'react';
import PropTypes from 'prop-types';

import './styles.css';

function Cell(props) {
  const elements = [
    () => {},
    () => {
      return <svg className="element" viewBox="0 0 40 40">
        <path className="element-x" d="M 10,10 L 30,30 M 30,10 L 10,30" />
      </svg>
    },
    () => {
      return <svg className="element" viewBox="0 0 100 100">
        <circle className="element-o" cx="50" cy="50" r="30" />
      </svg>
    }
  ];
  const el = elements[props.elementKey]();

  return <div className="cell" onClick={props.onClick}>
    {el}
  </div>;
}

Cell.propTypes = {
  onClick: PropTypes.func,
  elementKey: PropTypes.string.isRequired
}

export default Cell;
