import React from 'react';
import PropTypes from 'prop-types';

import Board from '../../containers/board';
import Menu from '../../containers/menu';
import './styles.css';

function Game(props) {
  let menu = '';

  if (props.menuIsOpened || props.winner || props.isDraw) {
    menu = <Menu />
  }

  return <div className="game-container">
    <Board>
    </Board>
    {menu}
  </div>;
}

Game.propTypes = {
  menuIsOpened: PropTypes.bool.isRequired,
  winner: PropTypes.string,
  isDraw: PropTypes.bool.isRequired
}

export default Game;
