import React from 'react';
import PropTypes from 'prop-types';

import Cell from '../cell'
import './styles.css';

function Menu(props) {
  let winner = '';
  let draw = '';

  if (props.winner) {
    winner = <div>
      <Cell elementKey={props.winner}></Cell>
      won!
    </div>
  } else if (props.isDraw) {
    draw = <div>DRAW</div>
  }

  return <div className="menu">
    <div className="message">
      {winner}
      {draw}
    </div>
    <div className="button" onClick={props.newGameClick}>New game</div>
  </div>;
}

Menu.propTypes = {
  newGameClick: PropTypes.func.isRequired,
  winner: PropTypes.string,
  isDraw: PropTypes.bool.isRequired
}

export default Menu;
