import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore } from 'redux'

import GameContainer from './containers/game'
import reducers from './reducers'
import registerServiceWorker from './registerServiceWorker';

let store = createStore(reducers)

ReactDOM.render(
  <Provider store={store}>
    <GameContainer />
  </Provider>, document.getElementById('root'));
registerServiceWorker();
